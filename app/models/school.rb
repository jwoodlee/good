class School < ActiveRecord::Base
  attr_accessible :location, :name

  geocoded_by :location
  after_validation :geocode, :if => :location_changed?

  has_many :chapters
  has_many :approvals
  has_many :users, :through         => :chapters
  has_many :donations, :through     => :chapters
  has_many :volunteerings, :through => :chapters
  has_many :organizations, :through => :chapters
  has_many :blogs, :through         => :chapters

  def sum_donations
    self.donations.sum(:value)
  end

  def sum_volunteerings
    self.volunteerings.sum(:hours)
  end

  def sum_activities
    self.donations.size + self.volunteerings.size
  end

  def approved_activities
    self.donations.approved + self.volunteerings.approved
  end

  def legitimacy_ratio
    self.approvals.present? ? (approved_activities.to_f / sum_activities.to_f)*100 : 0
  end

  def sum_users
    self.users.size
  end

  def sum_chapters
    self.chapters.size
  end

  def average_donations
  	self.approvals.present? ? (self.donations.sum(:value).to_f / sum_users.to_f) : 0
  end

  def average_volunteerings
  	self.approvals.present? ? (self.volunteerings.sum(:hours).to_f / sum_users.to_f) : 0
  end

  def to_param
    [id, name.parameterize].join('-')
  end
end
