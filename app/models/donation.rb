class Donation < ActiveRecord::Base
  attr_accessible :beneficiary, :date, :purpose, :title, :value, :location, :longitude, :latitude, :user, :school_id, :school

  scope :ranked, order: 'value DESC'
  scope :approved, where(:approved => true)
  scope :unapproved, where(:approved => false)

  has_one :approval, :as => :approvable, :dependent => :destroy
  belongs_to :user

  geocoded_by :location
  	after_validation :geocode, :if => :location_changed?

  def approved?
    !!self.approval #bangbang returns boolean
  end

  def self.approved
    self.all.map(&:approved?).keep_if {|d| d==true}.size
  end

  def self.unapproved
    self.all.map(&:approved?).keep_if {|d| d==false}.size
  end

  def to_param
    [id, title.parameterize].join('-')
  end
end
