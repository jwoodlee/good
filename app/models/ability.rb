class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
     else
      can :read, :all
    end
  end

  def set_global_abilities(user)
    cannot :manage, :all
    can :read, :all
    # can manage their own record.
    can [:read, :udpate], User, :id => user.id
    can [:read, :create, :udpate], Blog, :id => user.id
    can [:read, :create, :udpate], Donation, :id => user.id
    can [:read, :create, :udpate], Volunteering, :id => user.id
  end

  def set_global_officer_abilities(user)
    cannot :manage, User
    cannot :manage, Chapter

    # if user.is_officer?
    #   can :manage, User, :chapter_id => user.chapter_id
    #   can :manage, Chapter, :id => user.chapter_id
    # end
  end

  def set_global_admin_abilities(user)
    can :manage, :all
  end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
end

