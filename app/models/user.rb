class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :role_ids, :as => :admin
  attr_accessible :oauth_expires_at, :name, :email,
                  :oauth_token, :provider, :uid,
                  :organization, :organization_id,
                  :chapter, :chapter_id, :school,
                  :school_id, :first_name, :last_name,
                  :password, :password_confirmation,
                  :remember_me, :confirmed_at#, :profile_name

  belongs_to :organization
  belongs_to :chapter
  belongs_to :school

  has_many :donations, :dependent => :destroy
  has_many :volunteerings, :dependent => :destroy
  has_many :blogs, :dependent => :destroy
  has_many :approvals, :dependent => :destroy

  validates :first_name, :presence => true
  validates :last_name, :presence => true
  validates :email, :presence => true
  validates :school, :presence => true

  # need a default scope ordered by value of donations. Descending.

  mount_uploader :image, ImageUploader
  # mount_uploader :csv, CsvUploader

  # after_create :add_profile_name
  # has_and_belongs_to_many :roles, :join_table => :users_roles

  # after_create :add_user_to_mailchimp
  # before_destroy :remove_user_from_mailchimp

  after_create :assign_role
  # after_create :add_name

  # override Devise method
  # no password is required when the account is created; validate password when the user sets one
  # validates_confirmation_of :password
  def to_param
    [id, name.parameterize].join('-') unless self.name.nil?
  end

  def password_required?
    if !persisted?
      !(password != "")
    else
      !password.nil? || !password_confirmation.nil?
    end
  end

  def _official?
    self.chapter.blank? || (self.organization.blank? && self.has_role?(:official))
  end

  def is_administrator?
    self.has_role? :admin or self.has_role? :official
  end

  def can_approve?(resource)
    self.has_role? :official and self.school.id == resource.school_id
  end

  def is_official?
    self.has_role? :official
  end

  def superuser?
    self.has_role? :admin and self.has_role? :official
  end

  def is_officer?
    self.has_role? :officer
  end

  def chapter_brother?(chapter)
    self.chapter == chapter
  end

  def chapter_officer?(chapter)
    chapter_brother?(chapter) && (self.is_officer? || self.has_role?(:admin))
  end

  def can_edit_chapter?(chapter)
    chapter_officer?(chapter) || self.superuser?
  end

  def can_edit_organization?
    self.superuser?
  end

  def gender
    self.organization.gender.blank? ? "member" : member_type
  end

  def member_type
    self.organization.gender.downcase == "fraternity" ? "brother" : "sister"
  end

  def sum_donations
    self.donations.sum(:value)
  end

  def sum_volunteerings
    self.volunteerings.sum(:hours)
  end

  # override Devise method
  def confirmation_required?
    false
  end

  # override Devise method
  def active_for_authentication?
    confirmed? || confirmation_period_valid?
  end

  def send_reset_password_instructions
    if self.confirmed?
      super
    else
      errors.add :base, "You must receive an invitation before you set your password."
    end
  end

  # new function to set the password
  def attempt_set_password(params)
    p = {}
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end

  # new function to determine whether a password has been set
  def has_no_password?
    self.encrypted_password.blank?
  end

  # new function to provide access to protected method pending_any_confirmation
  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def profile_name
    self.last_name.downcase
  end

  def gravatar_url
    email = self.email.strip.downcase
    # downcased_email = stripped_email.downcase
    hash = Digest::MD5.hexdigest(email)
    "http://gravatar.com/avatar/#{hash}"
  end

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid, :email)).first_or_initialize.tap do |user|
      user.provider         = auth.provider
      user.uid              = auth.uid
      user.name             = auth.info.name
      user.username         = auth.info.username
      user.image            = auth.info.image
      user.oauth_token      = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
    end
  end

  def registration_complete? # false if nil.
    !self.organization.nil? || !self.chapter.nil? || !self.school.nil?
  end

  def facebook
    @facebook ||= Koala::Facebook::API.new(oauth_token)
    block_given? ? yield(@facebook) : @facebook
  rescue Koala::Facebook::APIError => e
    logger.info e.to_s
    nil
  end

  class << self
    def import(file)
      # begin
        # spreadsheet = open_spreadsheet(file)
        # header = spreadsheet(row[1])
        CSV.foreach(file, :headers => true) do |row|
          u = User.new
          u.update_attributes!({
            :organization => Organization.find_or_create_by_name(row[0]),
            :chapter    => Organization.find_or_create_by_name(row[1]),
            :school     => School.find_or_create_by_name(row[2]),
            :email      => row[3],
            :first_name => row[4],
            :last_name  => row[5]
          })
        end
      # rescue
      #   "ERROR!"
      # end
    end
  end

  private

  def add_name
  end

  def assign_role
    self.add_role :user if self.roles.first.nil?
  end

  def remove_role(role, *args)
    target = args.first.presence

    #target is nil when appropriate
    role = Role.where(:name => role, :member_id => self.id, :rollable => target).first
    role.delete
  end

  # def add_profile_name
    # self.profile_name = self.username.downcase.strip
    # User.pluck(:last_name).lowercase = self.profile_name
  # end

  #  def add_user_to_mailchimp
  #   return if email.include?(ENV['ADMIN_EMAIL'])
  #   mailchimp = Gibbon.new
  #   result = mailchimp.list_subscribe({
  #     :id => ENV['MAILCHIMP_LIST_ID'],
  #     :email_address => self.email,
  #     :double_optin => false,
  #     :update_existing => true,
  #     :send_welcome => true
  #     })
  #   Rails.logger.info("Subscribed #{self.email} to MailChimp") if result
  # rescue Gibbon::MailChimpError => e
  #   Rails.logger.info("MailChimp subscribe failed for #{self.email}: " + e.message)
  # end

  # def remove_user_from_mailchimp
  #   mailchimp = Gibbon.new
  #   result = mailchimp.list_unsubscribe({
  #     :id => ENV['MAILCHIMP_LIST_ID'],
  #     :email_address => self.email,
  #     :delete_member => true,
  #     :send_goodbye => false,
  #     :send_notify => true
  #     })
  #   Rails.logger.info("Unsubscribed #{self.email} from MailChimp") if result
  # rescue Gibbon::MailChimpError => e
  #   Rails.logger.info("MailChimp unsubscribe failed for #{self.email}: " + e.message)
  # end
end
