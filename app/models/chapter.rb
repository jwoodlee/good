class Chapter < ActiveRecord::Base
  attr_accessible :mission, :name, :school, :organization, :location, :longitude, :latitude



  geocoded_by :location
  after_validation :geocode, :if => :location_changed?

  belongs_to :organization
  belongs_to :school
  has_many :users
  has_many :donations, :through => :users
  has_many :volunteerings, :through => :users
  has_many :blogs, :through => :users

  def approvals
    self.donations + self.volunteerings
  end

  def legitimacy_ratio
    self.approvals.present? ? (approved_activities.to_f / sum_activities.to_f)*100 : 0
  end

  def sum_activities
    self.donations.size + self.volunteerings.size
  end

  def approved_activities
    self.approved_volunteerings + self.approved_donations
  end

  def approved_volunteerings
    self.volunteerings.approved
  end

  def approved_donations
    self.donations.approved
  end

  def sum_donations
  	self.donations.sum(:value)
  end

  def sum_volunteerings
  	self.volunteerings.sum(:hours)
  end

  def sum_members
    self.users.size
  end

  def to_param
    [id, name.parameterize].join('-')
  end

  def average_donations
    self.donations.present? ? (self.sum_donations.to_f / sum_members.to_f) : 0
  end

  def average_volunteerings
    self.volunteerings.present? ? (self.sum_volunteerings.to_f / sum_members.to_f) : 0
  end
end
