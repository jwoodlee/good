class Organization < ActiveRecord::Base
  attr_accessible :gender, :name, :orientation, :location, :longitude, :latitude

  geocoded_by :location
	after_validation :geocode, :if => :location_changed?

  validates :gender, :presence => true

  has_many :chapters
  has_many :users # what? don't think is right.
  has_many :users,         :through => :chapters
  has_many :schools,       :through => :chapters
  has_many :donations,     :through => :users
  has_many :volunteerings, :through => :users
  has_many :blogs,         :through => :users

  class << self
    def import(csv)
      count = 0
      CSV.foreach(csv, :headers => true) do |row|
        o = Organization.new
        o.update_attributes!({ :name => (row[1]), :gender => row[2], :orientation => row[3], :location => row[4] })
        count += 1
      end
      p "#{count} organizations imported."
    end
  end

  def approvals
    self.chapters.collect(&:approvals)
  end

  def sum_donations
    self.donations.sum(:value)
  end

  def sum_volunteerings
    self.volunteerings.sum(:hours)
  end

  def to_param
    [id, name.parameterize].join('-')
  end

  def members
    self.chapters.sum(&:sum_members)
  end

  def sum_activities
    self.chapters.sum(&:sum_activities)
  end

  def approved_activities
    approved_volunteerings + approved_donations
  end

  def approved_volunteerings
    self.chapters.sum(&:approved_volunteerings)
  end

  def approved_donations
    self.chapters.sum(&:approved_donations)
  end

  def legitimacy_ratio
    self.approvals.present? ? (approved_activities.size / sum_activities.to_f)*100 : 0
  end
end
