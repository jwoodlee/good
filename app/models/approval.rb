class Approval < ActiveRecord::Base
  attr_accessible :comments, :donation, :donation_id, :volunteering, :volunteering_id, :volunteering, :user, :user_id, :school_id, :school

  belongs_to :approvable, :polymorphic => true

  belongs_to :user
  belongs_to :school
end

=begin

  class Approval
    belongs_to :approvable, :polymorphic => true
  end

  class Donation
    has_one :approval, :as => :approvable
  end

  class Volunteering
    has_one :approval, :as => :approvable
  end

=end
