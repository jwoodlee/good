class Blog < ActiveRecord::Base
  attr_accessible :body, :title, :user, :school_id, :school

  validates :title, :presence => true

  belongs_to :user

  def to_param
    [id, title.parameterize].join("-")
  end
end
