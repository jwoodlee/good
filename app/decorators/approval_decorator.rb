class ApprovalDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def tr_class
    object.approved? ? "success" : "warning"
  end

  def approval_status
    object.approved? ? "Approved" : "Unapproved"
  end

  def status_class
    object.approved? ? "label label-success" : "label label-warning"
  end

  def _resource
    object.donation || object.volunteering
  end

  def link_to_manage
    if object.approved?
      (h.link_to(h.raw("Manage approval &rarr;").html_safe, object.approval, :class=>"btn btn-mini btn-success manage"))
    else
      (h.link_to(h.raw("Review activity &rarr;").html_safe, object, :class=>"btn btn-mini btn-warning right manage"))
    end
  end
end
