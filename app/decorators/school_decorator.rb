class SchoolDecorator < Draper::Decorator
  decorates_finders
  delegate_all

  def sum_donations
    h.number_to_currency(object.sum_donations)
  end

  def average_donations
    h.number_to_currency(object.average_donations)
  end

  def legitimacy_ratio
    "#{object.legitimacy_ratio}%"
  end
end
