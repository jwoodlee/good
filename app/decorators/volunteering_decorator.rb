class VolunteeringDecorator < Draper::Decorator
	delegate_all

	def status
		object.approved? ? "Approved" : "Pending Approval"
	end

	def label_class
		object.approved? ? "label-success" : "label-warning"
	end
end