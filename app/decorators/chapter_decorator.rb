class ChapterDecorator < Draper::Decorator
  decorates_finders
  delegate_all

  def member_type
    object.users.present? ? ("#{object.users.first.gender.capitalize}:") : "Member"
  end

  def average_donations
    h.number_to_currency(object.average_donations)
  end

  def average_volunteerings
    object.average_volunteerings
  end

  def total_raised
    # h.number_to_currency(object.sum_donations)
    "#{h.number_to_currency(object.sum_donations)} raised total."
  end

  def total_hours
    "#{object.sum_volunteerings.to_i} hours committed total"
  end

  def percentage_of_organization_funds
    "(#{((object.sum_donations / object.organization.sum_donations) * 100).to_i}% of organization funds raised)"
  end

  def percentage_of_organization_hours
    "(#{((object.sum_volunteerings / object.organization.sum_volunteerings) * 100).to_i}% of organization hrs committed)"
  end

  def unapproved_donations
    "#{object.school.donations.unapproved} new donations awaiting approval."
  end

  def unapproved_volunteerings
    "#{object.school.volunteerings.unapproved} new volunteerings awaiting approval."
  end

  def total_activities
    "#{object.school.sum_activities} activities posted to date at #{h.truncate(object.school.name, :length => 22)}."
  end

  def total_approved
    "#{object.school.approved_activities} total activities approved to date."
  end

  def legitimacy_ratio
    "Your legitimacy ratio is #{object.legitimacy_ratio}%."
  end

  def chapter_legitimacy
    "#{object.legitimacy_ratio}%"
  end
end
