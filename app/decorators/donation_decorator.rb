class DonationDecorator < Draper::Decorator
	delegate_all

	def approved?
		!!object.approval
	end

	def status
		object.approved? ? "Approved" : "Pending Approval"
	end

	def label_class
		object.approved? ? "label-success" : "label-warning"
	end

	def approver_title
		object.approval.user.roles.first.name.titleize
	end

	def approver
		object.approval.user.full_name
	end
end
