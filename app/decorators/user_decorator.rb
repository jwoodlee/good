class UserDecorator < Draper::Decorator
  decorates_finders
	delegate_all

	def member_type
		object.gender.downcase == "brother" ? "brother" : "sister"
	end

  def info
    "#{object.name} (#{object.email})"
  end

  def role
    "(#{object.roles.first.name})"
  end

  def role_title
    object.roles.first.name.titleize
  end

  def total_raised # personal
    "#{h.number_to_currency(object.sum_donations)} raised total." unless object._official?
  end

  def total_hours # personal
    "#{object.sum_volunteerings} hours committed total." unless object._official?
  end

  def org_raised_total
    "#{h.number_to_currency(object.organization.sum_donations)} raised, organization-wide." unless object._official?
  end

  def org_hours_total
    "#{object.organization.sum_volunteerings} hours volunteered, total." unless object._official?
  end

  def chapter_raised_total
    "#{h.number_to_currency(object.chapter.sum_donations)} raised, chapter total." unless object._official?
  end

  def chapter_hours_total
    "#{object.chapter.sum_volunteerings} hours, total." unless object._official?
  end

  def school_raised_total
    "#{h.number_to_currency(object.school.sum_donations)} raised all time, schoolwide."
  end

  def school_hours_total
    "#{object.school.sum_volunteerings} hours volunteered, schoolwide."
  end

  def percentage_of_chapter_funds
    "(#{(object.sum_donations / object.chapter.sum_donations).to_i * 100}%, chapter funds raised)" unless object._official?
  end

  def percentage_of_chapter_hours
    "(#{(object.sum_volunteerings / object.chapter.sum_volunteerings).to_i * 100}%, chapter hrs committed)" unless object._official?
  end

  def unapproved_donations
    "#{object.school.donations.unapproved} new donations awaiting approval."
  end

  def unapproved_volunteerings
    "#{object.school.volunteerings.unapproved} new volunteerings awaiting approval."
  end

  def total_activities
    "#{object.school.sum_activities} activities posted to date."
  end

  def total_approved
    "#{object.school.approved_activities} total activities approved to date."
  end

  def legitimacy_ratio
    "Schoolwide legitimacy ratio: #{object.school.legitimacy_ratio}%."
  end

  def org_legitimacy_ratio
    "(#{object.organization.legitimacy_ratio}% legitimate)"
  end

  def school_legitimacy_ratio
    "(#{object.school.legitimacy_ratio}% legitimate)"
  end
end
