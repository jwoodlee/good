class OrganizationDecorator < Draper::Decorator
  decorates_finders
	delegate_all

  def sum_organizations
    "(#{object.size} total organizations)"
  end

  def legitimacy_ratio
    "#{object.legitimacy_ratio}%"
  end
end
