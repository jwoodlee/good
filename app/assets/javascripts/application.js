// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require angular
//= require select2
//= require twitter/bootstrap
//= require gmaps
//= require_tree .


$(document).ready(function(){
  var bodyHeight = $("body").height();
  var viewPortHeight = $(window).height();

  if (viewPortHeight > bodyHeight) {
    $("footer").slideDown("slow").css("position", "absolute").css("bottom", -6);
  }

  $('.addField').hide();
  $('.addResource').click(function(e){
    e.preventDefault();
    $('.addField').slideDown('slow');
  });

  $('.closeMe').click(function(e){
    e.preventDefault();
    $('.addField').slideUp('slow');
  });

  $('.alert').delay(1500).fadeOut(500);

  $('.user-roster').hide();
  $('.hide-feed').click(function(e){
    e.preventDefault();
    $('.feed').hide();
    $('.user-roster').show();
  });

  $('.hide-user').click(function(e){
    e.preventDefault();
    $('.user-roster').hide();
    $('.feed').show();
  });

  $('.user-email').hide()
  $('i.icon-user').hide()
  $('.show-email').click(function(e){
    e.preventDefault();
    $('i.icon-envelope').hide();
    $('.user-email').show();
    $('i.icon-user').show()
    $('.user-name').hide();
  });

  $('i.icon-user').click(function(e){
    e.preventDefault();
    $('i.icon-user').hide()
    $('.user-email').hide();
    $('i.icon-envelope').show();
    $('.user-name').show();
  });

  $('.show-chapters').click(function(e){
    e.preventDefault();
    $('.school-chapters').show();
    $('.approvals-activity').hide();
  });

  $('.show-activity').click(function(e){
    e.preventDefault();
    $('.school-chapters').hide();
    $('.approvals-activity').show();
  });

});


// load social sharing scripts if the page includes a Twitter "share" button
// function loadSocial() {
//     //Twitter
//     if (typeof (twttr) != 'undefined') {
//       twttr.widgets.load();
//     } else {
//       $.getScript('http://platform.twitter.com/widgets.js');
//     }

//     //Facebook
//     if (typeof (FB) != 'undefined') {
//       FB.init({ status: true, cookie: true, xfbml: true });
//     } else {
//       $.getScript("http://connect.facebook.net/en_US/all.js#xfbml=1", function () {
//         FB.init({ status: true, cookie: true, xfbml: true });
//       });
//     }

//     //Google+
//     if (typeof (gapi) != 'undefined') {
//       $(".g-plusone").each(function () {
//         gapi.plusone.render($(this).get(0));
//       });
//     } else {
//       $.getScript('https://apis.google.com/js/plusone.js');
//     }
// }
