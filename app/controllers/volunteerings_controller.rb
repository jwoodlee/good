class VolunteeringsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :index]

   def index
    @volunteerings = Volunteering.order('created_at DESC')
  end

  def new
    @volunteering = Volunteering.new
  end

  def show
    @volunteering = Volunteering.find(params[:id]).decorate

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @volunteering }
    end
  end

  def edit
    @volunteering = Volunteering.find(params[:id])
  end

  def create
    @user = User.find(current_user.id)
    @volunteering = @user.volunteerings.new(params[:volunteering])
    @volunteering.school_id = @user.school.id

    respond_to do |format|
      if @volunteering.save
        format.html { redirect_to volunteerings_path, notice: 'Volunteering was successfully created.' }
        format.json { render json: @volunteering, status: :created, location: @volunteering }
      else
        format.html { render action: "new" }
        format.json { render json: @volunteering.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @volunteering = Volunteering.find(params[:id])

    respond_to do |format|
      if @volunteering.update_attributes(params[:volunteering])
        format.html { redirect_to @volunteering, notice: 'Volunteering was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @volunteering.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @volunteering = Volunteering.find(params[:id])
    @volunteering.destroy

    respond_to do |format|
      format.html { redirect_to volunteerings_url }
      format.json { head :no_content }
    end
  end
end
