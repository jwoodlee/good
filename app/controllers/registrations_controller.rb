# class RegistrationsController < ApplicationController

# 	def organizations_by_school
#     if params[:id].present?
#       @organizations = School.find(params[:id]).organizations
#     else
#       @organizations = []
#     end

#     respond_to do |format|
#       format.js
#     end
#   end

# 	def chapters_by_organization
#     if params[:id].present?
#       @chapters = Organization.find(params[:id]).chapters
#     else
#       @chapters = []
#     end

#     respond_to do |format|
#       format.js
#     end
#   end

# 	protected
# 	def after_sign_up_path_for(resource)
# 		account_path
# 	end
# end

class RegistrationsController < Devise::RegistrationsController

  # override #create to respond to AJAX with a partial
  # def create
  #   build_resource
  #   # build_resource(sign_up_params)

  #   Rails.logger.info "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~> #{build_resource.inspect}"
  #   Rails.logger.info "RRresource *********************> #{resource.inspect}"

  #   if resource.save
  #     if resource.active_for_authentication?
  #       # set_flash_message :notice, :signed_up if is_navigational_format?
  #       sign_in(resource_name, resource)
  #       # sign_up(resource_name, resource)
  #       # (render(:partial => 'thanks', :layout => false) && return)  if request.xhr?
  #       respond_with resource, :location => after_sign_up_path_for(resource)
  #     else
  #       # set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
  #       expire_session_data_after_sign_in!
  #       # (render(:partial => 'thankyou', :layout => false) && return)  if request.xhr?
  #       respond_with resource, :location => after_inactive_sign_up_path_for(resource)
  #     end
  #   else
  #     Rails.logger.info "}}}}}}}}}}}}}}}}}}~> ELSE #{resource.inspect}"
  #     clean_up_passwords resource
  #     render :action => :new, :layout => !request.xhr?
  #   end
  # end

  protected

  def after_inactive_sign_up_path_for(resource)
    # the page prelaunch visitors will see after they request an invitation
    # unless Ajax is used to return a partial
    thanks_path
  end

  def after_sign_up_path_for(resource)
    # the page new users will see after sign up (after launch, when no invitation is needed)
    redirect_to root_path
    # if current_user.has_role? :admin
    #   admin_dashboard_path
    # else
    #   performers_performer_dashboard_path
    # end
  end

  # def after_update_path_for(resource)
  #   if current_user.has_role? :admin
  #     admin_dashboard_path
  #   else
  #     performers_performer_dashboard_path
  #   end
  # end
end
