class ProfilesController < ApplicationController
  def show
  	@user = UserDecorator.find(params[:id]) rescue nil
  	if !@user._official?
  		donations = @user.donations
  		volunteerings = @user.volunteerings
  		blogs = @user.blogs
      approvals = @user.approvals.sort_by(&:created_at).reverse
      @feed = (donations + volunteerings + blogs + approvals).sort_by(&:created_at).reverse
  		render action: :show
  	elsif @user._official?
      @feed = @user.approvals.sort_by(&:created_at).reverse
    else
      redirect_to root_path, notice: "Sorry, could not locate that user."
	  end
  end
end
