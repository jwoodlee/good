class DonationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :index]

  def index
    @donations = Donation.ranked
  end

  def new
    @donation = Donation.new
  end

  def show
    @donation = Donation.find(params[:id]).decorate
  end

  def edit
    @donation = Donation.find(params[:id])
  end

  def create
    @user = User.find(current_user.id)
    @donation = @user.donations.new(params[:donation])
    @donation.school_id = @user.school.id

    respond_to do |format|
      if @donation.save
        format.html { redirect_to donations_path, notice: 'Donation was successfully documented. Thank you.' }
        format.json { render json: @donation, status: :created, location: @donation }
      else
        format.html { render action: "new" }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @donation = Donation.find(params[:id])

    respond_to do |format|
      if @donation.update_attributes(params[:donation])
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @donation = Donation.find(params[:id])
    @donation.destroy

    respond_to do |format|
      format.html { redirect_to donations_url }
      format.json { head :no_content }
    end
  end
end
