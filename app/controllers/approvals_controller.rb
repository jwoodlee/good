class ApprovalsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_permission
  before_filter :find_approval, :except  => [:index, :create]

  def index
    @user = current_user
    @approvals = @user.school.approvals
    @donations = @user.school.donations.scoped #TODO: scope to official's school.
    @volunteerings = @user.school.volunteerings #TODO: scope to official's school.
    @feed = @donations + @volunteerings
    @activities = {:donations => @donations.sort_by(&:created_at).reverse, :volunteerings => @volunteerings.sort_by(&:created_at).reverse}
  end

  def show
  end

  def new
    @approval = Approval.new
  end

  def edit
  end

  def create
    @user = current_user if current_user.is_administrator?
    resource = approvable_resource
    @approval = resource.create_approval! :school_id => @user.school.id, :user => @user

    # @approval.donation_id = @donation.id if @donation.present?
    # @approval.volunteering_id = @volunteering.id if @volunteering.present?
    # @approval.school_id = current_user.school.id

    respond_to do |format|
      if @approval.save
        format.html { redirect_to @approval, notice: 'Approval was successfully created.' }
        format.js#on { render json: @approval, status: :created, location: @approval }
      else
        format.html { render action: "new" }
        format.js#on { render json: @approval.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @approval.update_attributes(params[:approval])
        format.html { redirect_to @approval, notice: 'Approval was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @approval.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @approval.destroy

    respond_to do |format|
      format.html { redirect_to approvals_url }
      format.json { head :no_content }
    end
  end

  private
    def find_approval
      @approval = Approval.find(params[:id])#.decorate
    end

    def approvable_resource
      if params[:approvable_type] == "donation"
        Donation.find(params[:id])
      elsif params[:approvable_type] == "volunteering"
        Volunteering.find(params[:id])
      else
        nil #TODO: maybe redirect or render 404 or some shit
      end
    end
end
