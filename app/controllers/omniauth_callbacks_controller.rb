class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    @user = User.from_omniauth(request.env["omniauth.auth"]) #find_for_facebook_oauth

    if @user.persisted? && @user.active_for_authentication?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
    elsif @user.confirmation_required?
      redirect_to new_user_confirmation_path
      flash[:notice] = "Account confirmation required!"
    elsif @user.active_for_authentication?
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
      flash[:error] = "Please request an invite."
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
end
