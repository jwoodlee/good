class ApplicationController < ActionController::Base
  helper_method :is_owner?, :can_manage?, :has_permission?
  # check_authorization
  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
  	redirect_to root_path, :alert => exception.message
  end

  protected
    def after_sign_in_path_for(resource)
      # sign_in_url = url_for(:action => 'new', :controller => 'sessions', :only_path => false, :protocol => 'http')
      # if current_user.has_role?(:user)
      #   render :layout => "user_feed"
      unless current_user._official?
        feed_path
      else
        approvals_path
      end
    end

  private
  	def has_permission?
  		current_user.has_role? :admin or current_user.has_role? :official
  	end

  	def require_permission
  		redirect_to :root unless current_user.has_role? :admin or current_user.has_role? :official
  	end

    def admin_only
      redirect_to :root unless current_user.has_role? :admin
    end

  	def is_owner?(resource)
  		resource.user_id == current_user.id
  	end

    def can_manage?(resource)
      is_owner?(resource) if current_user.has_role? :admin
    end

	# def current_user
	#   @current_user ||= User.find(session[:user_id]) if session[:user_id]
	# end

	# def current_user?
		# !current_user.nil?
	# end

	# def authenticate_user
	# 	unless current_user?
	# 		flash[:warning] = "Must be signed in to access"
	# 		redirect_to root_path
	# 	end
	# end
end
