class PagesController < ApplicationController
  before_filter :authenticate_user!, :only => [:feed]
  before_filter :find_user, :only => [:feed]

  def index
    @ranked_orgs     = Organization.all.sort_by(&:sum_donations).reverse.take(7)
    @ranked_chapters = Chapter.all.sort_by(&:sum_donations).reverse.take(7)
    @school_hours    = School.all.sort_by(&:sum_volunteerings).reverse.take(7)

    @feed = (Donation.all + Volunteering.all + Blog.all).sort_by(&:created_at).reverse.take(13)
    flags = (@feed - Blog.all)
    @gmaps_options = {}
    @gmaps_options[:markers] = flags.collect { |f| { lat: f.latitude,
                                                     lng: f.longitude,
                                                    user: f.user.full_name,
                                                 chapter: f.user.chapter.name,
                                            organization: f.user.organization.name,
                                                    time: f.created_at.strftime('%A %l:%m %p') } }
    @gmaps_options[:lat] = (flags.first.try(:latitude) || 35.391289)
    @gmaps_options[:lng] = (flags.first.try(:longitude) || -78.425903)
    # raise @gmaps_options
  end

  # make a feed action where all users' relevant activity is shown. redirect there on login.
  # user, chapter, school, global
  def feed
    unless current_user._official?
      mine = @user.donations + @user.volunteerings + @user.blogs
      donations     = @user.donations.sort_by(&:created_at).reverse.take(2)
      volunteerings = @user.volunteerings.sort_by(&:created_at).reverse.take(2)
      blogs         = @user.blogs.sort_by(&:created_at).reverse.take(1)

      @personal     = (donations + volunteerings + blogs)

      my_chapter    = (@user.chapter.donations + @user.chapter.volunteerings + @user.chapter.blogs)
      c_donations     = @user.chapter.donations.sort_by(&:created_at).reverse.take(5)
      c_volunteerings = @user.chapter.volunteerings.sort_by(&:created_at).reverse.take(5)
      c_blogs         = @user.chapter.blogs.sort_by(&:created_at).reverse.take(5)

      @chapter      = ((c_donations + c_volunteerings + c_blogs) - mine)

      my_organization = (@user.organization.donations + @user.organization.volunteerings + @user.organization.blogs)
      o_donations     = @user.organization.donations.sort_by(&:created_at).reverse.take(5)
      o_volunteerings = @user.organization.volunteerings.sort_by(&:created_at).reverse.take(5)
      o_blogs         = @user.organization.blogs.sort_by(&:created_at).reverse.take(5)

      @organization = (o_donations + o_volunteerings + o_blogs) - (mine + my_chapter)

      my_school       = (@user.school.donations + @user.school.volunteerings + @user.school.blogs)
      s_donations     = @user.school.donations.sort_by(&:created_at).reverse.take(5)
      s_volunteerings = @user.school.volunteerings.sort_by(&:created_at).reverse.take(5)
      s_blogs         = @user.school.blogs.sort_by(&:created_at).reverse.take(5)

      @school = (s_donations + s_volunteerings + s_blogs) - (mine + my_chapter)

      @feed = (Donation.all + Volunteering.all + Blog.all - (mine + my_chapter + my_organization + my_school)).sort_by(&:created_at).reverse.take(10)
    else
      @feed = (Donation.all + Volunteering.all + Blog.all).sort_by(&:created_at).reverse.take(12)
    end
  end

  def about
  	@donations = Donation.all
  	@volunteerings = Volunteering.all
  end

  def thanks
  end

  protected
    def find_user
      begin
        @user = current_user #User.find(current_user.id)
      rescue ActiveRecord::RecordNotFound
        redirect_to :root, notice: "Please sign in or contact administrator!"
      end
    end

    def sort_by_created(num)
      s = sort_by(&:created_at).reverse
      s.take(num) unless num.nil?
    end
end
