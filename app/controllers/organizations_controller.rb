class OrganizationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]
  before_filter :find_organization, :except => [:index, :create]

  def index
    @organizations = Organization.all.sort_by(&:sum_donations).reverse
  end

  def show
  end

  def edit
  end

  def new
    @organization = Organization.new
  end

  def create
    @organization = Organization.new(params[:organization])

    respond_to do |format|
      if @organization.save
        format.html { redirect_to @organization, notice: 'Organization was successfully created.' }
        format.json { render json: @organization, status: :created, location: @organization }
      else
        format.html { render action: "new" }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    if @organization.update_attributes(params[:organization])
      format.html { redirect_to @organization, notice: 'Organization was successfully updated.' }
      format.json { head :no_content }
    else
      format.html { render action: "edit" }
      format.json { render json: @organization.errors, status: :unprocessable_entity }
    end
  end

  def destroy
    @organization.destroy

    respond_to do |format|
      format.html { redirect_to organizations_url }
      format.json { head :no_content }
    end
  end

  private
    def find_organization
      @organization = Organization.find(params[:id]).decorate
    end
end
