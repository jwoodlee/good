class ChaptersController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  def index
    @organizations = Organization.all.sort_by(&:sum_donations).reverse
    @chapters = Chapter.all.sort_by(&:sum_donations).reverse
  end

  def show
    @chapter = Chapter.find(params[:id]).decorate
    donations = @chapter.donations.order('created_at desc')
    volunteerings = @chapter.volunteerings.order('created_at desc')
    blogs = @chapter.blogs.order('created_at desc')
    @feed = (blogs + donations + volunteerings).sort_by { |f| f.created_at }.reverse
  end

  def new
    @chapter = Chapter.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @chapter }
    end
  end

  def edit
    @chapter = Chapter.find(params[:id])
  end

  def create
    # @user = User.find(current_user.id)
    @organization = Organization.find(params[:organization_id])
    @chapter = @organization.chapters.new(params[:chapter])

    respond_to do |format|
      if @chapter.save
        format.html { redirect_to @chapter, notice: 'Chapter was successfully created.' }
        format.json { render json: @chapter, status: :created, location: @chapter }
      else
        format.html { render action: "new" }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @chapter = Chapter.find(params[:id])

    respond_to do |format|
      if @chapter.update_attributes(params[:chapter])
        format.html { redirect_to @chapter, notice: 'Chapter was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @chapter = Chapter.find(params[:id])
    @chapter.destroy

    respond_to do |format|
      format.html { redirect_to chapters_url }
      format.json { head :no_content }
    end
  end
end
