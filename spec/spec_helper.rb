require 'rubygems'
require 'spork'
require 'factory_girl_rails'

ENV["RAILS_ENV"] ||= 'test'

Spork.prefork do
  require File.expand_path("../../config/environment", __FILE__)
  # require 'simplecov'
  # require 'simplecov-rcov'
  # SimpleCov.start 'rails' do
  #   SimpleCov.coverage_dir "spec/coverage"
  #   use_merging true
  #   formatter SimpleCov::Formatter::RcovFormatter
  # end

  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  require 'capybara/rspec'
  # require 'capybara/poltergeist'
  require 'draper/test/rspec_integration'

  Dir[Rails.root.join("spec/support/**/*.rb")].each { |file| require file }

  # WebMock.disable_net_connect! :allow_localhost => true

  RSpec.configure do |c|
    c.include FactoryGirl::Syntax::Methods
    # c.extend ControllerMacros, :type => :controller

    c.use_transactional_fixtures = false
    c.infer_base_class_for_anonymous_controllers = false
    c.filter_run_excluding :broken => true

    c.include Capybara::DSL

    c.order = "random"

    c.before :all do
      DeferredGarbageCollection.start
    end

    c.after :all do
      DeferredGarbageCollection.reconsider
    end
  end

  FactoryGirl.reload  
end

