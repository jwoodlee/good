RSpec.configure do |c|
	c.before :suite do
		ActiveRecord::Base.establish_connection Rails.configuration.database_configuration["#{Rails.env}"]
		DatabaseCleaner.strategy = :truncation
	end

	c.before :each do
		ActiveRecord::Base.establish_connection Rails.configuration.database_configuration["#{Rails.env}"]
		DatabaseCleaner.start
	end

	c.after :each do
		ActiveRecord::Base.establish_connection Rails.configuration.database_configuration["#{Rails.env}"]
		DatabaseCleaner.clean
	end

	c.around :each, :testing_transactions => true do |ex|
		DatabaseCleaner.strategy = nil
		ex.run
		DatabaseCleaner.strategy = :truncation
	end
end