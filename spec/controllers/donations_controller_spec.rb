require 'spec_helper'

describe DonationsController do
	let!(:donation_attrs) { attributes_for :donation }

	describe "GET 'index'" do
		it "assigns donations as @donations" do
			donation = Donation.create! donation_attrs
			get :index
			assigns(:donations).should eq([donation])
		end
	end

	describe "POST 'create'" do
	end

	describe "GET 'new'" do
	end

	describe "GET 'show'" do
	end

	describe "PUT 'update'" do
	end

	describe "DELETE 'destroy'" do
	end
end
