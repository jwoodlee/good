=begin
require 'spec_helper'

describe Chapter do
  it { should have_many :users }
  it { should have_many(:donations).through(:users) }
  it { should have_many(:volunteerings).through(:users) }
  it { should have_many(:blogs).through(:users) }

  it { should belong_to :organization }
  it { should belong_to :school }

  let!(:chapter_attrs)  { attributes_for :chapter  }
  let!(:chapter) { create :chapter }
  let!(:chapter) { create :chapter_with_donations }

  describe "#sum_donations" do
    it { p chapter_with_donations }
  end

  describe "#sum_volunteerings" do
  end

  describe "#sum_members" do
  end
end

  belongs_to :organization
  belongs_to :school
  has_many :users
  has_many :donations, :through => :users
  has_many :volunteerings, :through => :users
  has_many :blogs, :through => :users


  def sum_donations
    self.donations.sum(:value)
  end

  def sum_volunteerings
    self.volunteerings.sum(:hours)
  end

  def sum_members
    self.users.count
  end

  def to_param
    [id, name.parameterize].join('-')
  end
=end
