# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    provider "Facebook"
    uid "MyString"
    first_name "Frederick-Southgate"
    last_name  "Taylor"
    name "#{first_name} #{last_name}"
    oauth_token "MyString"
    oauth_expires_at "2013-05-08 20:44:33"
    confirmed_at DateTime.now

    factory :chapter_with_donations do
      after(:create) do |chapter|
        FactoryGirl.create_list(:donation, 5, :chapter => chapter)
      end
    end

    factory :chapter_with_volunteerings do
      after(:create) do |chapter|
        FactoryGirl.create_list(:volunteering, 5, :chapter => chapter)
      end
    end

    factory :chapter_with_blogs do
      after(:create) do |chapter|
        FactoryGirl.create_list(:blog, 5, :chapter => chapter)
      end
    end

  end
end
