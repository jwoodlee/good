# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :approval do
    comment "MyText"
    donation_id 1
    volunteerings_id 1
  end
end
