# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :donation do
    beneficiary "Ronald McDonald House"
    date        Time.now
    purpose     "Raise money for the children of San Francisco's RMH, with terminal cancer."
    title       "Baseball Championship Fundraiser"
    value       50.00
    location    "1640 Scott St. San Francisco, CA 94115"
    school_id   1
  end
end
