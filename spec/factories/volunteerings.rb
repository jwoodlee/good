# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :volunteerings do
    beneficiary "Habitat for Humanity"
    date        Time.now
    purpose     "Rebuild a back porch for victims of Hurricane Sandy."
    title       "Project Rebuild, Restart, Re-Up"
    house       5.0
    location    "Des Moines, IA"
    school_id   3
  end
end
