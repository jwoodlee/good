FactoryGirl.define do
  factory :chapter do
    name         "Beta Tau"
    location     "Raleigh, NC"
    mission      "Lorem ipsum dolor sit amet"
    school_id     1
    organization_id 1

    factory :chapter_with_users do
      after(:create) do |chapter|
        FactoryGirl.create_list(:user, 5, :chapter => chapter)
      end
    end
  end
end


=begin
  attr_accessible :mission, :name, :school, :organization, :location, :longitude, :latitude

  belongs_to :organization
  belongs_to :school
  has_many :users
  has_many :donations, :through => :users
  has_many :volunteerings, :through => :users
  has_many :blogs, :through => :users


  def sum_donations
    self.donations.sum(:value)
  end

  def sum_volunteerings
    self.volunteerings.sum(:hours)
  end

  def sum_members
    self.users.count
  end

  def to_param
    [id, name.parameterize].join('-')
  end
=end
