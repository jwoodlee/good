Feature: Registration
	
	Scenario: Successful New User Registration
		Given I don't have an account
		When I complete task with valid credentials
		And I receive the confirmation email
		And I navigate back to site
		Then I land on the create-account page
		And I update my school
		And I update my organization
		And I update my chapter