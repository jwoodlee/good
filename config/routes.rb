Greek::Application.routes.draw do

  devise_for :users, :controllers => { :registrations => "registrations", :confirmations => "confirmations", :omniauth_callbacks => "omniauth_callbacks" }

  authenticated :user do
    root :to => "pages#index"
  end

  get 'users/bulk_invite/:quantity' => 'users#bulk_invite', :via => :get, :as => :bulk_invite

  scope "/admin" do
    resources :users, :only => [:index, :new] do
      collection { post :import }
      get 'invite', :on => :member
      # resources :import_s, :only => :create
    end
  end

  scope "/official" do
    resources :approvals
    post ':approvals/new' => 'approvals#create', :as => :create_approval
  end

  scope "/officer" do
    resources :chapters, :only => [:show, :edit]
  end

  resources :organizations do
    resources :chapters
  end
  resources :chapters
  resources :schools
  resources :volunteerings
  resources :donations
  resources :blogs

  as :user do
    get 'request-invite', to: 'devise/registrations#new', :via => :post, :as => :invite
    get 'login',    to: 'devise/sessions#new',      :as => :login
    # get 'user/confirmation' => 'confirmations#update', :via => :put, :as => :update_user_confirmation
    put 'user/confirmation' => 'confirmations#update', :as => :update_user_confirmation
    get 'account', to: 'devise/registrations#edit', :as => :account
    delete 'logout' => 'devise/sessions#destroy',  :as => :logout
  end

  get 'auth/:provider/callback', to: 'devise/sessions#create'
  get 'auth/failure', to: redirect('/')

  get "home"  => 'pages#index', as: :home
  get "about" => 'pages#about', as: :about
  get "feed"  => 'pages#feed',  as: :feed

  root :to => "pages#index"

  # get '/registration', to: 'users#registration'
  # put '/registration', to: 'users#update'
  get 'users/:id', to: 'profiles#show', as: :profile
  get 'profiles/:id', to: 'profiles#show', as: :profile
  get 'thanks', to: 'pages#thanks'#, as: :thanks

  match '*path' => redirect('/'), via: :get
end
