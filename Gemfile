source 'https://rubygems.org'
# core
ruby '2.0.0'
gem 'rails', '3.2.12'
gem 'json'
gem 'geocoder', :git => 'git://github.com/alexreisner/geocoder.git'

# auth
gem 'devise'#, :git => "git://github.com/plataformatec/devise.git"
gem 'omniauth-facebook', '1.4.0' # version 1.4.1 seems to break with the current version of the Facebook SDK
gem "cancan"
gem "rolify"
gem 'koala'
gem 'draper'
gem 'hashie', '2.0.5'
gem 'carrierwave'
gem 'rmagick'

# Style gems
group :assets do
	gem 'uglifier', '>= 1.0.3'
	gem 'sass-rails', '~> 3.2.3'
	# gem 'formtastic-bootstrap'
	gem 'sassy-buttons'
	gem 'font-awesome-rails'
	gem 'susy'
	gem 'compass-rails', '1.0.3'
	gem 'therubyracer'
	gem 'twitter-bootstrap-rails'
	gem 'less-rails'
	gem 'angularjs-rails'
	gem 'select2-rails'
end
# Gems used only for assets and not required
# in production environments by default.

gem "simple_form"
gem 'jquery-rails'
gem 'haml-rails'
gem 'coffee-rails'
gem 'will_paginate'
# gem 'thin', :group => [:development, :test]

group :development, :test do
	gem 'sqlite3'
	gem 'better_errors'
	gem 'binding_of_caller'
	gem 'guard-rspec'
	gem 'guard-spork'
	gem 'guard-livereload'
	gem 'guard-bundler'
	gem 'rb-inotify', :require => false
	gem 'rb-fsevent', :require => false
	gem 'rb-fchange', :require => false
	gem 'simplecov', :require => false
	gem 'simplecov-rcov', :require => false
end

group :production do
	gem 'pg'
	# gem 'mysql2', '0.3.11'
end

gem "rspec-rails", ">= 2.11.4", :group => [:development, :test]
gem 'shoulda-matchers', :group => :test
gem "database_cleaner", ">= 0.9.1", :group => :test
gem "cucumber-rails", ">= 1.3.0", :group => :test, :require => false
gem "capybara", ">= 2.0.1", :group => :test
gem "email_spec", ">= 1.4.0", :group => :test
gem "launchy", ">= 2.1.2", :group => :test
gem "factory_girl_rails", ">= 4.1.0", :group => [:development, :test]
gem "gibbon"
gem "rubber"
gem "figaro"
gem "pry"
gem "roo"

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'
