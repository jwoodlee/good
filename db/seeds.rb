o = Organization.all
c = Chapter.all
s = School.all
u = User.all
r = Role.all
a = Approval.all
p "####=> #{o.count} organizations, #{c.count} chapters, #{s.count} schools, #{u.count} users, #{r.count} roles, #{a.count} being deleted..."
Organization.destroy_all
Chapter.destroy_all
School.destroy_all
User.destroy_all
Role.destroy_all
Approval.destroy_all
p "####=> deletion complete."

Organization.import("#{Rails.root}/lib/assets/organizations.csv")

School.create!({:name => "East Carolina University", 					:location => "Greenville, NC"})
School.create!({:name => "University of Virginia", 					    :location => "Charlottesville, VA"})
School.create!({:name => "Davidson College",  						       :location => "Davidson, NC"})
School.create!({:name => "The College of William and Mary", 			:location => "Williamsburg, VA"})
School.create!({:name => "North Carolina State University", 			:location => "Raleigh, NC"})
School.create!({:name => "University of North Carolina", 				 :location => "Durham, NC"})
School.create!({:name => "University of North Carolina at Wilmington", 	:location => "Wilmington, NC"})
School.create!({:name => "University of North Carolina Asheville", 		:location => "Asheville, NC"})

Chapter.create!({:mission => "Mission statement", :name => "Alpha",    :school => School.find_or_create_by_name!("University of Virginia"), 					:organization => Organization.find_or_create_by_name!("Pi Kappa Alpha"), :location => "Charlottesville, VA"})
Chapter.create!({:mission => "Mission statement", :name => "Beta",     :school => School.find_or_create_by_name!("Davidson College"),  						:organization => Organization.find_or_create_by_name!("Pi Kappa Alpha"), :location => "Davidson, NC"})
Chapter.create!({:mission => "Mission statement", :name => "Gamma",    :school => School.find_or_create_by_name!("The College of William and Mary"), 			:organization => Organization.find_or_create_by_name!("Pi Kappa Alpha"), :location => "Williamsburg, VA"})
Chapter.create!({:mission => "Mission statement", :name => "Beta Tau", :school => School.find_or_create_by_name!("North Carolina State University"), 			:organization => Organization.find_or_create_by_name!("Sigma Nu"), 		:location => "Raleigh, NC"})
Chapter.create!({:mission => "Mission statement", :name => "Gamma",    :school => School.find_or_create_by_name!("University of North Carolina"), 				:organization => Organization.find_or_create_by_name!("Sigma Nu"), 		:location => "Durham, NC"})
Chapter.create!({:mission => "Mission statement", :name => "Mu Gamma", :school => School.find_or_create_by_name!("University of North Carolina at Wilmington"), 	:organization => Organization.find_or_create_by_name!("Pi Kappa Alpha"), 		:location => "Wilmington, NC"})
Chapter.create!({:mission => "Mission statement", :name => "Mu Eta",   :school => School.find_or_create_by_name!("University of North Carolina Asheville"), 		:organization => Organization.find_or_create_by_name!("Sigma Nu"), 		:location => "Asheville, NC"})
Chapter.create!({:mission => "Mission statement", :name => "Epsilon Mu",:school => School.find_or_create_by_name!("East Carolina University"),	:organization => Organization.find_or_create_by_name!("Pi Kappa Alpha"), :location => "Charlottesville, VA"})

%w{admin official officer user}.each { |role| p Role.find_or_create_by_name! role }

@fst = User.create!({
  :name 		=> ENV['ADMIN_NAME'].dup,
  :first_name 	=> "Frederick",
  :last_name 	=> "Taylor",
  :organization => Organization.find_by_name("Pi Kappa Alpha"),
  :school 		=> School.find_by_name("East Carolina University"),
  :chapter 		=> Chapter.find_by_name("Epsilon Mu"),
  :email 		=> ENV['ADMIN_EMAIL'].dup,
  :password 	=> ENV['ADMIN_PASSWORD'].dup,
  :password_confirmation => ENV['ADMIN_PASSWORD'].dup,
  :confirmed_at => Time.now
})

@ec = User.create!({
  :name => "Ethan Carr",
  :first_name => "Ethan",
  :last_name => "Carr",
  :organization => Organization.find_or_create_by_name!("Sigma Nu"),
  :school => School.find_or_create_by_name!("North Carolina State University"),
  :chapter => Chapter.find_by_name("Beta Tau"),
  :email => "ethancarr1421@gmail.com",
  :password 	=> ENV['ADMIN_PASSWORD'].dup,
  :password_confirmation => ENV['ADMIN_PASSWORD'].dup,
  :confirmed_at => Time.now
})

@sd = User.create!({
  :name => "Sheldon Brown",
  :first_name => "Sheldon",
  :last_name => "Brown",
  :school => School.find_or_create_by_name!("North Carolina State University"),
  :email => "sheldon@ncstate.edu",
  :password 	=> ENV['ADMIN_PASSWORD'].dup,
  :password_confirmation => ENV['ADMIN_PASSWORD'].dup,
  :confirmed_at => Time.now
})

@user = User.create!({
  :name 	  => "Jameson Conrad",
  :first_name => "Jameson",
  :last_name  => "Conrad",
  :organization => Organization.first,
  :chapter => Chapter.first,
  :school => School.find_or_create_by_name!("University of North Carolina at Wilmington"),
  :email => "michaelcwiggins@hotmail.com",
  :password => ENV['ADMIN_PASSWORD'].dup,
  :password_confirmation => ENV['ADMIN_PASSWORD'].dup,
  :confirmed_at => Time.now
})

[@fst, @ec].each { |user| ["admin", "official", "officer"].each { |role| user.roles << Role.find_by_name(role); user.save! } }
@sd.roles << Role.find_by_name("official")
@sd.save!
@user.roles << Role.all.sort.last
@user.save!

p "User: #{@fst.name}, role: #{@fst.roles.collect(&:name)}"
p "User: #{@ec.name}, role: #{@ec.roles.collect(&:name)}"
p "User: #{@sd.name}, role: #{@sd.roles.collect(&:name)}"
p "User: #{@user.name}, role: #{@user.roles.collect(&:name)}"
p "user count: #{User.count}"
p "role count: #{Role.count}"
p "#{Role.all.collect &:name }"

5.times do |i|
  Donation.create!({
    :value => "45.00",
    :beneficiary => "Grace Community Homeless Shelter",
    :location => "Asheville, NC",
    :date => DateTime.now,
    :title => "Homeless Shelter Friday Fundraiser",
    :purpose => "To put a roof over the head of the less fortunate.",
    :user => @fst,
    :school_id => @fst.school.id
  })

   Donation.create!({
    :value => "75.00",
    :beneficiary => "Grace Community Homeless Shelter",
    :location => "Charlotte, NC",
    :date => DateTime.now,
    :title => "Homeless Shelter #{Time.now.strftime("%A")} Fundraiser",
    :purpose => "To put a roof over the head of the less fortunate.",
    :user => @ec,
    :school_id => @ec.school.id
  })

  Volunteering.create!({
   :beneficiary => "Ronald McDonald House",
   :hours       => 4,
   :location    => "Morehead City, NC",
   :purpose     => "Raise money for Charity",
   :sponsor     => "ECU Pirates",
   :title       => "Ronald McDonald Volunteering",
   :user    => @fst,
   :school_id => @fst.school.id
	})

  Volunteering.create!({
    :beneficiary => "Habitat for Humanity",
    :hours       => 4,
    :location    => "Manteo, NC",
    :purpose     => "Raise money for Charity",
    :sponsor     => "Bank of America",
    :title       => "Rebuild, Renew, Reup #{Time.now.year}",
    :user    => @ec,
    :school_id => @ec.school.id
	})

   Donation.create!({
    :value => "75.00",
    :beneficiary => "Grace Community Homeless Shelter",
    :location => "Knoxville, TN",
    :date => DateTime.now,
    :title => "Homeless Shelter #{Time.now.strftime("%A")} Fundraiser",
    :purpose => "To put a roof over the head of the less fortunate.",
    :user => @user,
    :school_id => @user.school.id
  })

  Volunteering.create!({
   :beneficiary => "Ronald McDonald House",
   :hours       => 4,
   :location    => "Richmond, VA",
   :purpose     => "Raise money for Charity",
   :sponsor     => "ECU Pirates",
   :title       => "Ronald McDonald Volunteering",
   :user    => @user,
   :school_id => @user.school.id
	})

  Blog.create!({:user => @user, :title => "#{i} Steps to Effective Fundraising", :school_id => School.find_or_create_by_name!("University of North Carolina at Wilmington").id})
  Blog.create!({:user => @fst, :title => "#{@fst.chapter.name.titleize}'s fundraising superbowl bash next Saturday.", :school_id => @fst.school.id})
  Blog.create!({:user => @ec, :title => "#{@ec.chapter.name} becomes first chapter at #{@ec.school.name} to raise #{i} dollars!",:school_id => School.find_or_create_by_name!("North Carolina State University").id})
end

