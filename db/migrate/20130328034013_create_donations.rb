class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.float :value
      t.string :beneficiary
      t.datetime :date
      t.string :title
      t.text :purpose

      # access all of a user's donations.
      t.references :user

      # access all approvals from school
      t.references :school, :null => false

      t.timestamps
    end
  end
end
