class CreateVolunteerings < ActiveRecord::Migration
  def change
    create_table :volunteerings do |t|
      t.string :beneficiary
      t.float :hours
      t.string :location
      t.text :purpose
      t.string :sponsor
      t.string :title
      t.datetime :date

      # access all of a user's volunteerings.
      t.references :user

      # access all approvals from school
      t.references :school, :null => false

      t.timestamps
    end
  end
end
