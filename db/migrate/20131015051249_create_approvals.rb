class CreateApprovals < ActiveRecord::Migration
  def up
    create_table :approvals do |t|

      # allow any comments official might have.
      t.text :comments

      # access information of approved information.
      t.integer :donation_id
      t.integer :volunteering_id

      # access all approvals from school
      t.references :school, :null => false

      # tag the official who approved.
      # access all of a user's approvals.
      t.references :user

      # implement polymorphism
      t.references :approvable, :polymorphic => true

      # tag the time the official approved.
      t.timestamps
    end
  end

  def down
  	drop_table :approvals
  end
end
