class AddStuffToUsers < ActiveRecord::Migration
  def up
    # add_column    :users, :name, :string
    add_column    :users, :username, :string
    add_column    :users, :image, :string
    add_column    :users, :first_name, :string
    add_column    :users, :last_name, :string
    add_column    :users, :school_id, :integer
    add_column    :users, :organization_id, :integer
    add_column    :users, :chapter_id, :integer
  end

  def down
    # remove_column :users, :name, :string
    remove_column :users, :username, :string
    remove_column :users, :image, :string
    remove_column :users, :first_name, :string
    remove_column :users, :last_name, :string
		remove_column :users, :school_id, :integer
		remove_column :users, :organization_id, :integer
    remove_column :users, :chapter_id, :integer
  end
end
