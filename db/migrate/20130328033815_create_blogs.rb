class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :body

      # access all of a user's blogs.
      t.references :user

      # access all approvals from school
      t.references :school, :null => false

      t.timestamps
    end
  end
end
