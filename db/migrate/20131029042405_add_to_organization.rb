class AddToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :contact_email, :string
    add_column :organizations, :tax_id, :string
    add_column :organizations, :founded, :string
    add_column :organizations, :colors, :string
    add_column :organizations, :motto, :string
  end
end
