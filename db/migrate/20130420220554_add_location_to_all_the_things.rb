class AddLocationToAllTheThings < ActiveRecord::Migration
  def up
  	add_column :organizations, :location, :string
  	add_column :chapters, :location, :string
  	add_column :donations, :location, :string
  	add_column :organizations, :longitude, :float
  	add_column :organizations, :latitude, :float
  	add_column :chapters, :longitude, :float
  	add_column :chapters, :latitude, :float
  	add_column :donations, :longitude, :float
  	add_column :donations, :latitude, :float
    add_column :volunteerings, :longitude, :float
    add_column :volunteerings, :latitude, :float
    # add_column :schools, :longitude, :float
    # add_column :schools, :latitude, :float
  end

  def down
  	remove_column :organizations, :location, :string
  	remove_column :chapters, :location, :string
  	remove_column :donations, :location, :string
  	remove_column :organizations, :longitude, :float
  	remove_column :organizations, :latitude, :float
  	remove_column :chapters, :longitude, :float
  	remove_column :chapters, :latitude, :float
  	remove_column :donations, :longitude, :float
  	remove_column :donations, :latitude, :float
  	remove_column :volunteerings, :longitude, :float
  	remove_column :volunteerings, :latitude, :float
    remove_column :schools, :longitude, :float
    remove_column :schools, :latitude, :float
  end
end
