class AddSchoolToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :school_id, :integer
  end
end
