class AddImage < ActiveRecord::Migration
  def up
    add_column :donations,     :image, :string
    add_column :volunteerings, :image, :string
    add_column :schools,        :image, :string
    add_column :organizations,  :image, :string
    add_column :chapters,       :image, :string
  end

  def down
    remove_column :users,         :image, :string
    remove_column :donations,     :image, :string
    remove_column :volunteerings, :image, :string
    remove_column :schools,        :image, :string
    remove_column :organizations,  :image, :string
    remove_column :chapters,       :image, :string
  end
end
