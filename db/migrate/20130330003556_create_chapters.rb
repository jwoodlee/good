class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.integer :organization_id
      t.string :name
      t.text :mission

      t.timestamps
    end
  end
end
