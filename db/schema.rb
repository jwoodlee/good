# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131201022212) do

  create_table "approvals", :force => true do |t|
    t.text     "comments"
    t.integer  "donation_id"
    t.integer  "volunteering_id"
    t.integer  "school_id",       :null => false
    t.integer  "user_id"
    t.integer  "approvable_id"
    t.string   "approvable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "blogs", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "user_id"
    t.integer  "school_id",  :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "chapter_id"
  end

  create_table "chapters", :force => true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.text     "mission"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.integer  "school_id"
    t.string   "location"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "image"
  end

  create_table "charities", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "donations", :force => true do |t|
    t.float    "value"
    t.string   "beneficiary"
    t.datetime "date"
    t.string   "title"
    t.text     "purpose"
    t.integer  "user_id"
    t.integer  "school_id",   :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "location"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "image"
  end

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.string   "gender"
    t.string   "orientation"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "location"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "contact_email"
    t.string   "tax_id"
    t.string   "founded"
    t.string   "colors"
    t.string   "motto"
    t.string   "image"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "schools", :force => true do |t|
    t.string   "name"
    t.string   "location"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.float    "longitude"
    t.float    "latitude"
    t.string   "image"
  end

  create_table "sponsors", :force => true do |t|
    t.string   "name"
    t.string   "address"
    t.string   "image"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username"
    t.string   "image"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "school_id"
    t.integer  "organization_id"
    t.integer  "chapter_id"
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "volunteerings", :force => true do |t|
    t.string   "beneficiary"
    t.float    "hours"
    t.string   "location"
    t.text     "purpose"
    t.string   "sponsor"
    t.string   "title"
    t.datetime "date"
    t.integer  "user_id"
    t.integer  "school_id",   :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.float    "longitude"
    t.float    "latitude"
    t.string   "image"
  end

end
