require 'csv'

desc "Import CSV Data"
task :create_organizations => :environment do
	file = File.read("#{Rails.root}/lib/tasks/organizations.csv")
  CSV.foreach(file, headers: true) do |line|
    puts "Importing #{line.inspect}"
    
    line = Organization.create!({
	    :id => line[0],
	    :name => line[1].downcase,
	    :party => line[2].downcase,
	    :region => line[3].downcase,
    })
    line.save
  end
end