INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (1, "Zeta Tau Alpha"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (2,"Acacia","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (3,"Alpha Chi Rho","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (4,"Alpha Delta Gamma","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (5,"Alpha Delta Phi","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (6,"Alpha Epsilon Pi","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (7,"Alpha Gamma Rho","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (8,"Alpha Gamma Sigma","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (9,"Alpha Kappa Lambda","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (10,"Alpha Phi Alpha","Fraternity","Social", "3200 Market Street, Burbank, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (11,"Alpha Phi Delta","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (12,"Alpha Sigma Phi","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (13,"Alpha Tau Omega","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (14,"Beta Chi Theta","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (15,"Beta Sigma Psi","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (16,"Beta Theta Pi","Fraternity","Social", "3200 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (17,"Chi Phi","Fraternity","Social", "2002 Main Street, Raleigh, NC", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (18,"Chi Psi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (19,"Delta Chi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (20,"Delta Kappa Epsilon","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (21,"Delta Phi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (22,"Delta Psi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (23,"Delta Sigma Phi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (24,"Delta Tau Delta","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (25,"Delta Upsilon","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (26,"FarmHouse","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (27,"Iota Nu Delta","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (28,"Iota Phi Theta","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (29,"Kappa Alpha Order","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (30,"Kappa Alpha Psi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (31,"Kappa Alpha Society","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (32,"Kappa Delta Phi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (33,"Kappa Delta Rho","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (34,"Lambda Chi Alpha","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (35,"Lamda Phi Epsilon","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (36,"Lambda Sigma Upsilon","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (37,"Lambda Theta Phi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (38,"Nu Alpha Kappa","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (39,"Omega Delta Phi","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (40,"Phi Beta Sigma","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (41,"Phi Gamma Delta","Fraternity","Social", "2002 3rd Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (42,"Phi Iota Alpha","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (43,"Phi Kappa Psi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (44,"Phi Kappa Sigma","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (45,"Phi Kappa Tau","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (46,"Phi Kappa Theta","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (47,"Phi Lambda Chi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (48,"Phi Mu Delta","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (49,"Phi Sigma Kappa","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (50,"Phi Sigma Phi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (51,"Pi Kappa Alpha","Fraternity","Social", "8347 West Range Cove, Memphis, TN", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (52,"Pi Kappa Phi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (53,"Pi Lambda Phi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (54,"Psi Upsilon","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (55,"Sigma Alpha Epsilon","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (56,"Sigma Alpha Mu","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (57,"Sigma Beta Rho","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (58,"Sigma Chi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (59,"Sigma Lambda Beta","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (60,"Sigma Nu","Fraternity","Social", "9 North Lewis Street, Lexington, Virginia", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (61,"Sigma Phi Delta","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (62,"Sigma Phi Epsilon","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (63,"Sigma Phi ","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (64,"Sigma Pi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (65,"Sigma Tau Gamma","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (66,"Tau Delta Phi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (67,"Tau Epsilon Phi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (68,"Tau Kappa Epsilon","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (69,"Tau Phi Sigma","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (70,"Theta Chi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (71,"Theta Delta Chi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (72,"Theta Xi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (73,"Triangle","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (74,"Zeta Beta Tau","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (75,"Zeta Psi","Fraternity","Social", "3200 Market Street, San Francisco, CA", "Sun, May 12 2013 19:43:52 -0700", "Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (76,	"Alpha Chi Omega", "Sorority", "Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (77,	"Alpha Delta Pi"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (78,	"Alpha Gamma Delta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (79,	"Alpha Epsilon Phi"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (80,	"Alpha Omicron Pi"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (81,	"Alpha Phi"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (82,	"Alpha Sigma Alpha"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (83,	"Alpha Sigma Tau"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (84,	"Alpha Xi Delta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (85,	"Chi Omega"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (86,	"Delta Delta Delta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (87,	"Delta Gamma"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (88,	"Delta Zeta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (89,	"Delta Phi Epsilon"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (90,	"Gamma Phi Beta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (91,	"Kappa Alpha Theta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (92,	"Kappa Delta"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (93,	"Kappa Kappa Gamma"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (94,	"Phi Mu"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (95,	"Phi Sigma Sigma"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (96,	"Pi Beta Phi"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (97,	"Sigma Delta Tau"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (98,	"Sigma Kappa", "Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (99,	"Sigma Sigma Sigma"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");
INSERT into ORGANIZATIONS (id, name, gender, orientation, location, created_at, updated_at)
	VALUES (100, "Theta Phi Alpha"	,"Sorority","Social", "3200 Market Street, San Francisco, CA","Sun, May 12 2013 19:43:52 -0700","Sun, May 12 2013 19:43:52 -0700");