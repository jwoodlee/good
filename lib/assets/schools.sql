INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (1, "East Carolina University", "Greenville, NC", "Mon, 01 Apr 2013 08:29:10 -0700", "Mon, 01 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (2, "University of Virgina", "Charlottesville, VA", "Mon, 01 Apr 2013 08:29:10 -0700", "Mon, 01 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (3, "North Carolina State University", "Raleigh, NC", "Sun, 20 Apr 2013 08:29:10 -0700", "Sun, 20 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (4, "University of North Carolina at Wilmington", "Raleigh, NC", "Sun, 20 Apr 2013 08:29:10 -0700", "Sun, 20 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (5, "University of North Carolina at Chapel Hill", "Chapel Hill, NC", "Sun, 20 Apr 2013 08:29:10 -0700", "Sun, 20 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (6, "Duke University", "Durham, NC", "Sun, 20 Apr 2013 08:29:10 -0700", "Sun, 20 Apr 2013 08:29:10 -0700");
INSERT into SCHOOLS (id, name, location, created_at, updated_at)
VALUES (7, "University of North Carolina at Asheville", "Asheville, NC", "Sun, 20 Apr 2013 08:29:10 -0700", "Sun, 20 Apr 2013 08:29:10 -0700");