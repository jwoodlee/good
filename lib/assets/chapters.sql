INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (1, 1, 51, "Epsilon Mu", "Greenville, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (2, 2, 51, "Alpha", "Charlottesville, VA","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (3, 1, 60, "Beta Tau", "Raleigh, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (4, 6, 60, "Gamma", "Durham, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (5, 5, 60, "Psi", "Greenville, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (6, 4, 60, "Mu Gamma", "Wilmington, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");
INSERT into CHAPTERS (id, school_id, organization_id, name, location, created_at, updated_at)
VALUES (7, 7, 60, "Mu Eta", "Asheville, NC","Sun, 07 Apr 2013 08:29:10 -0700", "Sun, 07 Apr 2013 08:29:10 -0700");